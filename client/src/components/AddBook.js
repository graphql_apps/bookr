import React, { Component } from "react";
import { graphql, compose } from "react-apollo";

import { getAuthorsQuery, getBooksQuery } from '../queries/queries'
import { addBookMutation } from "../queries/mutations";

class AddBook extends Component {
  state = {
    name: '',
    genre: '',
    authorId: ''
  }

  displayAuthors() {
    let { loading, error, authors } = this.props.getAuthorsQuery
    
    if (loading) return (<option disabled>Loading authors...</option>)
    if (error) return (<option>Error: {error.message}</option>)

    return (
      authors.map(author => <option 
        key={author.id}
        value={author.id}
        >{ author.name }</option>)
    )
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })

    return this.state
  }

  submitForm = (e) => {
    e.preventDefault()
    this.props.addBookMutation({
      variables: {
        name: this.state.name,
        genre: this.state.genre,
        authorId: this.state.authorId
      },
      refetchQueries: [
        { query: getBooksQuery }
      ]
    })
  }
  
  render() {
    return (
      <form 
        id="add-book"
        onSubmit={ this.submitForm }
      >
        <div className="field">
          <label>Book Name:</label>
          <input 
            type="text" 
            placeholder="Book Name" 
            name="name"
            onChange={ this.handleChange }
            autoFocus 
          />
        </div>
        <div className="field">
          <label>Genre:</label>
          <input 
            type="text" placeholder="Genre"
            name="genre" 
            onChange={this.handleChange}
          />
        </div>
        <div className="field">
          <label>Author:</label>
          <select 
            name="authorId"
            onChange={this.handleChange}
          >
            <option>Select Author</option>
            { this.displayAuthors() }
          </select>
        </div>

        <button>+</button>
      </form>
    )
  }
}

export default compose(
  graphql(getAuthorsQuery, { name: "getAuthorsQuery" }),
  graphql(addBookMutation, { name: 'addBookMutation' })
)(AddBook);
import React, { Component } from 'react'
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from 'react-apollo'

// Import Components
import BookList from './BookList'
import AddBook from './AddBook'

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql'
  // uri: 'http://192.168.43.220:4000/graphql'
})

class App extends Component {
  render() {
    return (
      <ApolloProvider client={ client }>
        <div id="main">
          <h1>Your Bookr Reading List</h1>
          <BookList />
          <AddBook />
        </div>
      </ApolloProvider>
    );
  }
}

export default App;

import React, { Component } from "react";
import { graphql } from "react-apollo";

import { getBookQuery } from '../queries/queries'

class BookDetails extends Component {
  displayBookDetails () {
    const { book } = this.props.data

    if (book) {
      return (
        <div>
          <h2 className="title">{book.name}</h2>
          <p className="genre"><span>Genre: </span>{book.genre}</p>
          <p className="author"><span>written by </span>{book.author.name}</p>

          <div className="meta-books">
            <p className="lead">All Books by {book.author.name}:</p>

            <ul>
              {
                book.author.books.map(book => {
                  return <li key={book.id} className="other-books">{book.name}</li>
                })
              }
            </ul>
          </div>
        </div>
      )
    } else {
      return(
        <div>No book selected...</div>
      )
    }
  }

  render() {
    console.log(this.props);
    return( <div id="book-details">{ this.displayBookDetails() }</div>);
  }
}

export default graphql(getBookQuery, {
  options: props => {
    return {
      variables: {
        id: props.bookId
      }
    }
  }
})(BookDetails)
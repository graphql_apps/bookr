import { gql } from "apollo-boost";

// Get Authors
const getAuthorsQuery = gql`
  {
    authors {
      name
      id
    }
  }
`;

// Get Books
const getBooksQuery = gql`
  {
    books {
      name
      id
      author {
        name
      }
    }
  }
`;

const getBookQuery = gql`
  query($id: ID!){
    book(id: $id) {
      id 
      name
      genre
      author {
        id
        name
        books {
          name
          id
          genre
        }
      }
    }
  }
`

export { getAuthorsQuery, getBooksQuery, getBookQuery };
require('./config/config')

const express = require('express')
const graphqlHTTP = require('express-graphql')
const cors = require('cors')
const mongoose = require('mongoose')

// import schema
const schema = require('./schema/schema')
const port = process.env.PORT
const uri = process.env.MONGODB_URI

// Connect to the database
mongoose.connect(uri, {
  useCreateIndex: true,
  useNewUrlParser: true
}).then(() => console.log('Connected to the database'))
  .catch(err => console.log(`Could not connect to DB: ${err}`))

const app = express()
app.use(cors())

// Start up the GraphQL server
app.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true
}))

app.listen(port, () => console.log(`bookr is running on localhost:${port}/graphql`))
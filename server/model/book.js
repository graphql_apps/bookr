const mongoose = require('mongoose')
const Schema = mongoose.Schema

const bookSchema = new Schema({
  name: { type: String, required: true, unique: true },
  genre: { type: String, required: true },
  completed: { type: Boolean, default: false },
  desc: String,
  likes: { type: Number, default: 0 },
  dislikes: { type: Number, default: 0},
  authorId: String
})

module.exports = mongoose.model('Book', bookSchema)
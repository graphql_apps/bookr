const graphql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLID,
  GraphQLBoolean,
  GraphQLList
} = graphql

// Require Types
const { BookType, AuthorType, UserType} = require('../types/types')

// Require models
const Book = require('../model/book')
const Author = require('../model/author')
const User = require('../model/user')


const RootQuery = new GraphQLObjectType({
  name: 'RootQuery',
  description: "All the endpoints you need are embedded here...",
  fields: {
    // Query for a single book
    book: {
      description: 'Make Query for a single book with it\'s id',
      type: BookType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) }
      },
      resolve (parent, { id }) {
        //  Find the book and return it to the client
        return Book.findById(id)
      }
    },
    // Query for all books
    books: {
      description: 'Make a Query for all books, has no need for args',
      type: GraphQLList(BookType),
      resolve () {
        // Find all books
        return Book.find({})
      }
    },

    // Query for a single Author
    author: {
      description: 'Make Query for a single author with author\'s id or authors name',
      type: AuthorType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        name: { type: GraphQLString }
      },
      resolve (parent, { id }) {
        return Author.findById(id)
      }
    },
    // Query for all Authors
    authors: {
      description: 'Make a Query for all authors, has no need for args',
      type: GraphQLList(AuthorType),
      resolve (parent, args) {
        return Author.find({})
      }
    },
    
    // Query for a single User
    user: {
      description: 'Make Query for a single user with user\'s id or user\'s name or email',
      type: UserType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        name: { type: GraphQLString },
        email: { type: GraphQLString }
      },
      resolve (parent, { id }) {
        // Find the user and return user to the client
        return User.findById(id)
      }
    },
    // Query for all Users
    users: {
      description: 'Make a Query for all users, has no need for args',
      type: new GraphQLList(UserType),
      // type: UserType,
      resolve: () => {
        // Find all users
        return User.find({})
      }
    }
  }
})

// Mutations
const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  description: 'How would you like to interact, update, delete or create data?',
  fields: {
    // create new author
    addAuthor: {
      type: AuthorType,
      description: 'Add an author to the database',
      args: { 
        name: { type: new GraphQLNonNull(GraphQLString) }
      },
      resolve: (parent, { name }) => {
        let author = new Author({ name })

        return author.save()
      }
    },

    // create new user
    addUser: {
      type: UserType,
      description: 'Add a user to the database',
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        email: { type: new GraphQLNonNull(GraphQLString) },
        password: { type: new GraphQLNonNull(GraphQLString) }
      },
      resolve: (parent, { name, email, password}) => {
        let user = new User({
          name,
          email,
          password
        })

        return user.save()
        // user.save().then(doc => doc).catch(errors => errors)
      }
    },

    // create new book
    addBook: {
      type: BookType,
      description: 'Add a new book to the collection',
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        genre: { type: new GraphQLNonNull(GraphQLString) },
        completed: { type: GraphQLBoolean },
        desc: { type: GraphQLString },
        likes: { type: GraphQLInt },
        dislikes: { type: GraphQLInt },
        authorId: { type: GraphQLID }
      },
      resolve: (parent, {
        name, genre, completed, desc, authorId, likes, dislikes
      }) => {
        let book = new Book({
          name, genre, completed, desc, authorId, likes, dislikes
        })

        return book.save()
      }
    }
  }
})

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
})
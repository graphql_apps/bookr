const graphql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLID,
  GraphQLBoolean,
  GraphQLList
} = graphql

// Require models
const Book = require('../model/book')
const Author = require('../model/author')

// BookType
const BookType = new GraphQLObjectType({
  name: 'Book',
  description: 'The Book Type allows you make Query or Mutations for the book type. It has required fields that cannot be left blank',
  fields: () => ({
    id: { type: new GraphQLNonNull(GraphQLID) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    genre: { type: new GraphQLNonNull(GraphQLString) },
    completed: { type: GraphQLBoolean },
    desc: { type: GraphQLString },
    likes: { type: GraphQLInt },
    dislikes: { type: GraphQLInt },
    author: { 
      type: AuthorType,
      resolve (parent, args) {
        return Author.findById(parent.authorId)
      }
    }
  })
})

// AuthorType
const AuthorType = new GraphQLObjectType({
  name: 'Author',
  description: 'The Author is mostly associated with Books, This type is different from the UserType',
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    books: { 
      type: new GraphQLList(BookType),
      resolve (parent, args) {
        return Book.find({ authorId: parent.id})
      }
    }
  })
})

// UserType
const UserType = new GraphQLObjectType({
  name: 'User',
  description: 'The UserType refers to the registered users of the application. This is different from the AuthorType',
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    email: { type: GraphQLString },
    password: { type: GraphQLID },
  })
})

module.exports = {
  BookType,
  AuthorType,
  UserType
}